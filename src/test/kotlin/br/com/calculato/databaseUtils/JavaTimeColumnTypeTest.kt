package br.com.calculato.databaseUtils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.date.shouldBeBefore
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalTime

object TimeTest : Table() {
    val id = integer("id")
    val hora = time("hora")
}

class JavaTimeColumnTypeTest : StringSpec({
    "javatime test" {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")

        transaction {
            SchemaUtils.create(TimeTest)

            val timetestId = TimeTest.insert {
                it[id] = 1
                it[hora] = LocalTime.now()
            } get TimeTest.id

            timetestId shouldBe 1

            val hora = TimeTest.select { TimeTest.id eq 1 }.single()[TimeTest.hora]

            hora.shouldBeBefore(LocalTime.now())
        }
    }
})