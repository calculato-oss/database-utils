package br.com.calculato.databaseUtils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

private object TextArrayTable : Table() {
    val id = integer("id")
    val coluna = textarray("coluna")
}

class TextArrayColumnTypeTest : StringSpec({
    "textarray any" {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")

        transaction {
            SchemaUtils.create(TextArrayTable)

            TextArrayTable.insert {
                it[id] = 1
                it[coluna] = arrayOf("texo", "numero", "simbolo")
            }[TextArrayTable.id]

            TextArrayTable.insert {
                it[id] = 2
                it[coluna] = arrayOf("simbolo")
            }[TextArrayTable.id]

            TextArrayTable.insert {
                it[id] = 3
                it[coluna] = arrayOf("figura")
            }[TextArrayTable.id]

//            NOT SUPPORTED BY H2 DATABASES
            val simbolos = TextArrayTable.select {
                (TextArrayTable.id eq 1)
                    .and(TextArrayTable.coluna any emptyList<String>())
                    .and(TextArrayTable.id greaterEq 1)
            }.mapNotNull { it[TextArrayTable.id] }

            simbolos.size shouldBe 2
        }
    }
})

fun buildTagsCondition(tags: List<String>, column: Column<Array<String>>): Op<Boolean>? {
    var tagsCondition: Op<Boolean>? = null
    if (tags.isNotEmpty()) {
        tagsCondition = Op.build {
            column any tags.component1()

        }
        tags.drop(1).forEach {
            tagsCondition.or(column any it)
        }
    }

    return tagsCondition
}