package br.com.calculato.databaseUtils

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContain
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

private object ArrayTable : Table() {
    val id = integer("id")
    val coluna = integerarray("coluna")
}

class IntegerArrayColumnTypeTest : StringSpec({
    "integerarray test" {
        Database.connect("jdbc:h2:mem:test", driver = "org.h2.Driver", user = "root", password = "")

        transaction {
            SchemaUtils.create(ArrayTable)

            val id = ArrayTable.insert {
                it[id] = 1
                it[coluna] = arrayOf(1, 2, 6)
            }[ArrayTable.id]

            id shouldBe 1

            val coluna = ArrayTable.select { ArrayTable.id eq 1 }.single()[ArrayTable.coluna]

            coluna shouldHaveSize 3
            coluna shouldContain 6
        }
    }
})