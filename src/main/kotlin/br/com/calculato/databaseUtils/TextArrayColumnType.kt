package br.com.calculato.databaseUtils

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.ColumnType
import org.jetbrains.exposed.sql.Expression
import org.jetbrains.exposed.sql.ExpressionWithColumnType
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.OrOp
import org.jetbrains.exposed.sql.QueryBuilder
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.statements.jdbc.JdbcConnectionImpl
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.vendors.PostgreSQLDialect
import org.jetbrains.exposed.sql.wrapAsExpression

/**
 * Text array column type to use with modern databases
 */
class TextArrayColumnType : ColumnType() {
    override fun sqlType(): String = buildString {
        if (TransactionManager.current().db.dialect is PostgreSQLDialect)
            append("TEXT ")
        append("ARRAY")
    }

    override fun valueToDB(value: Any?): Any? {
        return if (value is Array<*>) (TransactionManager.currentOrNull()?.connection as JdbcConnectionImpl).connection.createArrayOf(
            "TEXT",
            value
        ) else super.valueToDB(value)
    }

    override fun valueFromDB(value: Any): Any {
        if (value is java.sql.Array) return value.array
        if (value is Array<*>) return value

        error("Array does not support for this database")
    }
}

/**
 * Function to register custom TEXT ARRAY with the given [name]
 */
fun Table.textarray(name: String): Column<Array<String>> = registerColumn(name, TextArrayColumnType())

class AnyOp(private val expr1: Expression<*>, private val expr2: String) : Op<Boolean>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        append("'$expr2' = ANY(")
        append(expr1)
        append(")")
    }
}

class AnyArrayOp(private val expr1: Expression<*>, private val expr2: List<String>) : Op<Boolean>() {
    override fun toQueryBuilder(queryBuilder: QueryBuilder) = queryBuilder {
        if (expr2.isEmpty()) {
            error("Cannot use an empty list to make an any array clause")
        }

        if(expr2.count() > 1) {
            append("(")
        }
        expr2.forEachIndexed { index, s ->
            if(index > 0) {
                append(" OR ")
            }
            append("'$s' = ANY(")
            append(expr1)
            append(")")
        }
        if(expr2.count() > 1) {
            append(")")
        }
    }
}

infix fun ExpressionWithColumnType<Array<String>>.any(string: String): Op<Boolean> = AnyOp(this, string)

infix fun ExpressionWithColumnType<Array<String>>.any(array: List<String>): Op<Boolean> = AnyArrayOp(this, array)