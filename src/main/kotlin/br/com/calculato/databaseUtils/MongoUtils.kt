package br.com.calculato.databaseUtils

import org.bson.types.ObjectId
import org.litote.kmongo.Id
import org.litote.kmongo.id.WrappedObjectId
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId

fun ObjectId.getLocalDate(): LocalDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()

fun Id<*>.getDateTime(): LocalDateTime = LocalDateTime.ofInstant(
    Instant.ofEpochSecond((this as WrappedObjectId).id.timestamp.toLong()),
    ZoneId.of("Brazil/East")
)

